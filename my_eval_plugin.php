<?php
/**
 * Plugin Name: My Eval Plugin
 * Description: Mon super plugin !
 * Author: Nicolas RAYNAUD
 */

// Hook the 'admin_menu' action hook, run the function named 'mfp_Add_My_Admin_Link()'
add_action( 'admin_menu', 'add_my_Admin_link' );

function add_my_Admin_link(){
    add_menu_page(
        'My First Page', // Title of the page
        'My First Plugin', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        plugin_dir_path(__FILE__) .'includes/mep-acp-page.php' // The 'slug' - file to display when clicking the link
    );
}

function get_my_result(){
    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM wp_annuaire ");
    echo ('<table>
        <thead>
        <th>Entreprise</th>
        <th>Localisation</th>
        <th>Prénom du contact</th>
        <th>Nom du contact</th>
        <th>Email de contact</th>
        </thead>
        <tbody>
        ');
    foreach ($results as $result) {
        echo ('<tr>
        <td>'.$result->nom_entreprise.'</td>
        <td>'.$result->localisation_entreprise.' </td>
        <td>'.$result->prenom_contact.'</td>
        <td>'.$result->nom_contact.'</td>
        <td>'.$result->mail_contact.'</td>
        </tr>
        ');
    }
    echo ('
    </tbody>
    </table>');
}

add_shortcode('result', 'get_my_result');
?>